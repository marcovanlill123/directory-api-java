# Directory API - ReadMe

Welcome to the Directory API! This API allows you to retrieve directory listings and perform health checks on your system.

## API Endpoints

The API exposes the following endpoints:

- `GET /healthcheck`: Retrieves the health status of the API. For more information, refer to the Swagger documentation.
- `POST /directory-listing`: Retrieves the directory listing based on the provided path. For more information, refer to the Swagger documentation.

Please refer to the Swagger documentation for detailed information about the request and response formats, as well as example usage.

## Documentation

The API documentation is available in Swagger format. You can access it by checking in the root folder of the program either in swaggerApiDocs.json or swaggerApiDocs.yaml. It provides detailed information about each endpoint, including request and response schemas.

## Running the Docker Image

To run the Directory API as a Docker container, follow these steps:

1. Make sure you have Docker installed on your machine. Refer to the Docker documentation for installation instructions specific to your operating system.

2. Build the Docker image by running the following command in the project directory:

   ```shell
   docker build -t java-api .
3. Once the image is built successfully, you can run the container using the following command:
    docker run -p 8080:8080 java-api
4. The API should now be accessible at http://localhost:8080. You can use tools like cURL or Postman to interact with the endpoints.

## Prerequisites

Before running the Directory API, make sure you have the following prerequisites installed:

Java Development Kit (JDK) 17 or higher
Apache Maven
Docker (if you want to run the application as a Docker container)

## Installation

To install and run the Directory API locally, follow these steps:

1. Clone the repository to your local machine:
   git clone https://gitlab.com/marcovanlill123/directory-api-java.git

2. Navigate to the project directory:
   cd directory-api

3. Build the application using Maven:
   mvn clean install

4. Once the build is successful, you can run the application using the following command:
   mvn spring-boot:run

## Unit testing

unit tests can be found and run in th unitTest.java file. There are 2 unit tests , one for the directory endpoint and another for the health check endpoint.


## build the container
 docker build -t java-api .

## mount the container to C drive:
docker run -v C:/:/host -p 8080:8080 container_image_name

## or

## mount the container to entire host system
docker run -v /:/host -p 8080:8080 container_image_name

## Postman Usage when running through maven:

You can just pass the exact directory you want the files to be returned from inside of the body of the post endpoint. For example:
"path": "C:/Program Files"

## Postman Usage when running thorugh docker image:

after you have mounted the host system, any path you want to pass has a /host prefix to it. If you mounted to the C drive, you would pass path in instead of C:/. For example:
"path": "host/Program Files"