package com.java_api.java_api;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
class unitTest {

	@Test
	void getApiHealth() {
		var controller = new controller();
		controller.api();
		assertEquals("I am healthy and running!", controller.api());
	}
	@Test
    void testGetDirectoryListing() throws Exception {

        DirectoryListingRequest request = new DirectoryListingRequest();
        request.setPath("C:/Users/Public/Documents");
        
        List<FileInfo> expectedDirectoryListing = new ArrayList<>();
        expectedDirectoryListing.add(new FileInfo("desktop.ini", "C:\\Users\\Public\\Documents\\desktop.ini", 278, "-rwx"));
        expectedDirectoryListing.add(new FileInfo("My Music", "C:\\Users\\Public\\Documents\\My Music", 0, "-rwx"));
        expectedDirectoryListing.add(new FileInfo("My Pictures", "C:\\Users\\Public\\Documents\\My Pictures", 0, "-rwx"));
        expectedDirectoryListing.add(new FileInfo("My Videos", "C:\\Users\\Public\\Documents\\My Videos", 0, "-rwx"));
        expectedDirectoryListing.add(new FileInfo("test.txt", "C:\\Users\\Public\\Documents\\test.txt", 59, "-rwx"));

        DirectoryListingResponse expectedResponse = new DirectoryListingResponse();
        expectedResponse.setFiles(expectedDirectoryListing);
        expectedResponse.setError(null);
    
        controller controller = new controller();

        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    
        mockMvc.perform(MockMvcRequestBuilders.post("http://localhost:8080/directory-listing")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(request)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(new ObjectMapper().writeValueAsString(expectedResponse)));
    }
    

}
