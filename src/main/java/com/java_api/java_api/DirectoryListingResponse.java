package com.java_api.java_api;

import java.util.List;

public class DirectoryListingResponse {
    private List<FileInfo> files;
    private String error;

    public List<FileInfo> getFiles() {
        return files;
    }

    public void setFiles(List<FileInfo> files) {
        this.files = files;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}

