package com.java_api.java_api;

public class DirectoryListingRequest {
    private String path;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
