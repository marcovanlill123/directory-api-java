package com.java_api.java_api;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@RestController
public class controller {

    @GetMapping("/healthcheck")
    public String api() {
        return "I am healthy and running!";
    }

    @PostMapping("/directory-listing")
    public ResponseEntity<DirectoryListingResponse> getDirectoryListing(@RequestBody DirectoryListingRequest request) {
        String path = request.getPath();
    
        List<FileInfo> directoryListing = listDirectory(path);
    
        DirectoryListingResponse response = new DirectoryListingResponse();

        if (directoryListing == null) {
            response.setError("Unable to retrieve directory listing.");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        } else if (directoryListing.isEmpty()) {
            response.setError("No files found in the specified directory.");
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
        } else {
            response.setFiles(directoryListing);
            return ResponseEntity.ok(response);
        }
    }
    
    private List<FileInfo> listDirectory(String path) {
        List<FileInfo> directoryListing = new ArrayList<>();
    
        try {
            File directory = new File(path);
            if (directory.isDirectory()) {
                File[] files = directory.listFiles();
    
                if (files != null) {
                    for (File file : files) {
                        FileInfo fileInfo = new FileInfo();
                        fileInfo.setFileName(file.getName());
                        fileInfo.setFullPath(file.getAbsolutePath());
                        fileInfo.setFileSize(file.length());
    
                        String attributes = "-";
                        if (file.canRead()) {
                            attributes += "r";
                        }
                        if (file.canWrite()) {
                            attributes += "w";
                        }
                        if (file.canExecute()) {
                            attributes += "x";
                        }
                        fileInfo.setAttributes(attributes);
    
                        directoryListing.add(fileInfo);
                    }
                }
            } else {
                System.out.println("The provided path is not a directory: " + path);
            }
        } catch (Exception e) {
            System.out.println("Error retrieving directory listing: " + e.getMessage());
        }
    
        return directoryListing;
    }

}