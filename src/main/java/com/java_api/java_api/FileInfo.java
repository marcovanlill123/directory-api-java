package com.java_api.java_api;

public class FileInfo {
    private String fileName;
    private String fullPath;
    private long fileSize;
    private String attributes;

    public FileInfo() {
    }

    public FileInfo(String fileName, String fullPath, long fileSize, String attributes) {
        this.fileName = fileName;
        this.fullPath = fullPath;
        this.fileSize = fileSize;
        this.attributes = attributes;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFullPath() {
        return fullPath;
    }

    public void setFullPath(String fullPath) {
        this.fullPath = fullPath;
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public String getAttributes() {
        return attributes;
    }

    public void setAttributes(String attributes) {
        this.attributes = attributes;
    }
}
