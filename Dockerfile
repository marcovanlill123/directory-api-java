FROM openjdk:17-jdk-slim

WORKDIR /app

COPY . /app

RUN ./mvnw clean package -DskipTests

ENV HOST_DIRECTORY_PATH /host-directory

ENTRYPOINT ["java", "-jar", "/app/target/java_api-0.0.1-SNAPSHOT.jar"]

EXPOSE 8080
